// ==UserScript==
// @name         Github Reviewer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Show on which MR you interracted
// @author       SergioTaquine
// @match        /^https:\/\/\w(?=\S*['-])([a-zA-Z0-9-_]+)\/pull/g
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @grant        GM.listValues
// @downloadURL  https://gitlab.com/sergiotaquine/follow-my-github-reviews/raw/main/index.js
// @updateURL    https://gitlab.com/sergiotaquine/follow-my-github-reviews/raw/main/index.js
// ==/UserScript==

const DAYS_BEFORE_FLUSHING = 30; // How much time wee keep local saves
const MS_TO_DAY = 86400000; // How many miliseconds is 1 day
const { href: currentHref, pathname: currentPathName } = window.location || {};
const homeMergeRequestsPathName = currentPathName.substring(0, currentPathName.indexOf('pull') + "pull".length)+'s';

const getInteractions = async(updatedInteractionsList) => {
    const allMergeRequests = document.querySelectorAll('.js-issue-row');

    for (let mr of allMergeRequests) {
        const link = mr.getElementsByTagName('a')[0].href;
        const currentMergeRequest = updatedInteractionsList.find(({name}) => name.startsWith(link)) || {};
        const { value: { interactionsNumber, approved } = {} } = currentMergeRequest;
        if (interactionsNumber || approved){
            link.prepend(`${approved ? '✅': '❌' } 👨‍💻(${interactionsNumber})`);
        }
    }
}

const flushOldValues = async() => {
    // List all values and check if we have to remove some old stuffs (Decrease localStorage usage)
    const allValues = await GM.listValues();
    let currentValues = [];

    if (allValues && allValues.length > 0) {
        for (let value of allValues) {
            const valueParsed = JSON.parse(await GM.getValue(value));
            const { date } = valueParsed;
            if (Date.now() > date + (DAYS_BEFORE_FLUSHING * MS_TO_DAY)) {
                await GM.deleteValue(value);
            } else {
                currentValues.push({
                    name: value,
                    value: valueParsed
                });
            }
        }
    }
    return currentValues;
};

const listeningInteractions = async() => {
    const reviewButtonTextSelector = 'Submit review';
    const commentButtonNameSelector = 'single_comment';

    document.addEventListener('click', async(e) => {
        const button = e.target.closest("button");
        if (button) {
            const isComment = button.innerText === reviewButtonTextSelector || button.name === commentButtonNameSelector;
            const isSubmit = button.innerText === reviewButtonTextSelector;
            let isApproval = false;

            if (isSubmit) {
                // Scrap form values that shows the option checked in submitted form
                const form = button.closest('form');
                var formData = new FormData(form);
                isApproval = Object.fromEntries(formData)['pull_request_review[event]'] === 'approve';
            }

            const existingObject = await GM.getValue(currentHref);

            const { interactionsNumber = 0, approved } = existingObject ? JSON.parse(existingObject): {};

            const newObject = JSON.stringify({
                name: currentHref,
                date: Date.now(),
                interactionsNumber: isComment ? interactionsNumber + 1: interactionsNumber,
                approved: isApproval || approved
            });
            GM.setValue(currentHref, newObject);
        }
    });
};

(async() => {
    'use strict';

    const isHome = currentPathName === homeMergeRequestsPathName || currentPathName === homeMergeRequestsPathName+'/' ;

    if (isHome) {
        const updatedInteractionsList = await flushOldValues();
        getInteractions(updatedInteractionsList);
    } else {
        listeningInteractions();
    }
})();
